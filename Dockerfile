FROM node:alpine

RUN apk update
RUN apk upgrade

RUN npm install -g markdownlint-cli --force

RUN apk add gcc make musl-dev ruby ruby-dev
RUN gem install mdl
RUN gem install metadata-json-lint
RUN gem install puppet-lint
RUN gem install rails-erb-lint
RUN gem install yaml-lint
